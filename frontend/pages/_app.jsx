import dynamic from "next/dynamic";
import "../styles/globals.css";
import "../styles/admin.css";
import Head from "next/head";

const CLayoutAdmin = dynamic(() => import("../component/layout"));
function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <CLayoutAdmin>
        <Component {...pageProps} />
      </CLayoutAdmin>
    </>
  );
}

export default MyApp;
