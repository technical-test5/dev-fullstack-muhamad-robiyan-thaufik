import {
  DeleteOutlined,
  EditOutlined,
  ExclamationCircleOutlined,
} from "@ant-design/icons";
import { Button, Form, Modal, notification } from "antd";
import { useEffect, useState } from "react";
import CDropdownTable from "../../component/dropdown-table";
import CDatePicker from "../../component/form/date-picker";
import CInput from "../../component/form/input";
import CSelectOption from "../../component/form/select";
import CTable from "../../component/table";
import CTitle from "../../component/title";
import { MyApi } from "../api/hello";
import { useRouter } from "next/router";

const InvoiceLine = () => {
  const router = useRouter();
  const { confirm } = Modal;
  const showConfirm = () => {};

  const [errors, setErrors] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [listCustomer, setListCustomer] = useState([]);
  const [listPackage, setListPackage] = useState([]);
  const [paginate, setPaginate] = useState({
    current_page: "",
    pageSize: 15,
    keyword: "",
  });

  useEffect(() => {
    getData();
    getCustomerList();
    getPackageList();
  }, [paginate]);

  const getCustomerList = async () => {
    await MyApi.get("customer-list")
      .then((res) => setListCustomer(res.data))
      .catch((err) => errorNotification(err.response.data.message));
  };
  const getPackageList = async () => {
    await MyApi.get("package-list")
      .then((res) => setListPackage(res.data))
      .catch((err) => errorNotification(err.response));
  };

  const [data, setData] = useState();
  const [loading, setLoading] = useState(true);
  const [forms, setForms] = useState({
    id: "",
    customer_id: "",
    package_id: "",
    inv_number: "",
    inv_date: "",
    amount: 0,
    total_discount_amount: 0,
    total_amount: 0,
    sales_invoice_id: "",
    total: 0,
  });

  function onShowSizeChange(current, pageSize) {
    setPaginate({
      ...paginate,
      pageSize: pageSize,
      current_page: current,
    });
  }

  const getData = () => {
    setLoading(true);
    MyApi.get("invoice-line", {
      params: {
        page: paginate.current_page,
        keyword: paginate.keyword,
        per_page: paginate.pageSize,
      },
    })
      .then((r) => {
        setData(r.data.data);
        setForms({
          ...forms,
          total: r.data.total,
        });
      })
      .catch((err) => {
        errorNotification(err.response.data.message);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const redirectDetail = (evt, id) => {
    evt.preventDefault();
    router.push("invoice-line/" + id);
  };

  const columns = [
    {
      title: "Customer Name",
      key: "name",
      dataIndex: "name",
    },
    {
      title: "Invoice Number",
      key: "inv_number",
      render: (item) => (
        <a
          style={{ color: "#1890ff" }}
          href={"invoice-line/" + JSON.stringify(item.id)}
          onClick={(e) => redirectDetail(e, item.id)}
        >
          {item.inv_number}
        </a>
      ),
      width: 250,
    },
    {
      title: "Invoice Date",
      dataIndex: "inv_date",
      key: "inv_date",
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount",
      render: (item) => {
        return (
          "Rp. " + item.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
        );
      },
    },
    {
      title: "Total Discount Amount",
      dataIndex: "total_discount_amount",
      key: "total_discount_amount",
      render: (item) => {
        return (
          "Rp. " + item.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
        );
      },
      width: 165,
    },
    {
      title: "Total Amount",
      dataIndex: "total_amount",
      render: (item) => {
        return (
          "Rp. " + item.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
        );
      },
      key: "total_amount",
    },
    {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 100,
      render: (item) => (
        <CDropdownTable
          action={[
            {
              action: () => handleEdit(item),
              icon: <EditOutlined />,
              name: "Edit",
            },
            {
              action: () => handleRemove(item.id),
              icon: <DeleteOutlined />,
              name: "Delete",
            },
          ]}
        />
      ),
    },
  ];

  const handleEdit = (e) => {
    setForms({
      id: e.id,
      customer_id: e.customer_id,
      package_id: e.package_id,
      inv_number: e.inv_number,
      inv_date: e.inv_date,
      amount: e.amount,
      total_discount_amount: e.total_discount_amount,
      total_amount: e.total_amount,
      sales_invoice_id: e.sales_invoice_id,
    });
    setIsModalVisible(true);
  };

  const refresh = () => {
    setPaginate({
      current_page: "",
      pageSize: 15,
      keyword: "",
    });
    getData();
  };

  const showModal = () => {
    setIsModalVisible(true);
    setForms({
      id: "",
      customer_id: "",
      inv_number: "",
      inv_date: "",
      amount: 0,
      total_discount_amount: 0,
      total_amount: 0,
      package_id: "",
      sales_invoice_id: "",
    });
  };

  const cancel = () => {
    setIsModalVisible(false);
    setErrors("");
    setForms({
      id: "",
      customer_id: "",
      inv_number: "",
      inv_date: "",
      amount: 0,
      total_discount_amount: 0,
      total_amount: 0,
      package_id: "",
      sales_invoice_id: "",
    });
  };

  const onChangeDate = (value, dateString) => {
    // parameter value == Moment js
    setForms({
      ...forms,
      inv_date: dateString,
    });
  };
  const handleChange = (evt) => {
    const newData = { ...forms };
    newData[evt.target.id] = evt.target.value;
    setForms(newData);
  };

  function onCustomer(value) {
    setForms({
      ...forms,
      customer_id: value,
    });
  }
  function onPackage(value) {
    setForms({
      ...forms,
      package_id: value,
    });
    getAmount(value);
  }

  function onTotalDiscount(evt) {
    const newData = { ...forms };
    newData[evt.target.id] = evt.target.value;
    setForms(newData);
    total(evt.target.value);
  }

  const total = (evt) => {
    if (forms.amount > 0) {
      if (evt > forms.amount) {
        alert("Diskon tidak boleh lebih besar dari harga");
      } else {
        if (!isNaN(evt)) {
          setForms({
            ...forms,
            total_amount: parseInt(forms.amount - evt),
            total_discount_amount: parseInt(evt),
          });
        } else {
          alert("format harus angka");
        }
      }
    } else {
      if (!isNaN(evt)) {
        setForms({
          ...forms,
          total_discount_amount: parseInt(evt),
        });
      } else {
        alert("format harus angka");
      }
    }
  };

  const getAmount = async (id) => {
    await MyApi.get("package/" + id).then((res) => {
      setForms({
        ...forms,
        amount: res.data,
        package_id: id,
        total_amount: res.data - forms.total_discount_amount,
      });
    });
  };

  const handleSubmit = () => {
    MyApi({
      method: forms.id > 0 ? "put" : "post",
      data: forms,
      url: forms.id > 0 ? "invoice-line/" + forms.id : "invoice-line",
    })
      .then((res) => {
        setForms({});
        setIsModalVisible(false);
        successNotification(res.data.message);
        getData();
        setErrors("");
      })
      .catch((err) => {
        errorNotification(err.response.data.message);
        setErrors(err.response.data.errors);
        console.log(err.response.data.errors);
      });
  };

  const errorNotification = (msg) => {
    notification.error({
      message: "Error",
      description: msg,
    });
  };

  const successNotification = (msg) => {
    notification.success({
      message: "Succes",
      description: msg,
    });
  };

  const handleRemove = (id) => {
    showConfirm(
      confirm({
        title: "Do you want to delete these items?",
        content: "You can't undo this operation",
        icon: <ExclamationCircleOutlined />,
        okText: "Yes",
        okType: "danger",
        cancelText: "Cancel",
        onOk() {
          MyApi.delete("invoice-line/" + id)
            .then((r) => {
              errorNotification(r.data.message);
            })
            .catch((err) => {
              errorNotification(err.response.data.message);
            })
            .finally(() => {
              getData();
            });
        },
        onCancel() {},
      })
    );
  };

  return (
    <>
      <CTitle title="Invoice Line" />
      <CTable
        name="Invoice Line"
        total={forms.total}
        onShowSizeChange={onShowSizeChange}
        data={data}
        scroll={{ y: 300 }}
        columns={columns}
        searchValue={paginate.keyword}
        refresh={() => refresh()}
        loading={loading}
        addData={showModal}
      />
      <Modal
        title={forms.id > 0 ? "Edit Invoice" : "Add Invoice"}
        closable={false}
        visible={isModalVisible}
        footer={
          forms.id > 0
            ? [
                <Button
                  key="cancel"
                  style={{ backgroundColor: "#ffc107", color: "white" }}
                  onClick={() => cancel()}
                >
                  Cancel
                </Button>,
                <Button
                  key="update"
                  style={{ backgroundColor: "green", color: "white" }}
                  onClick={() => handleSubmit()}
                >
                  Update
                </Button>,
              ]
            : [
                <Button
                  key="back"
                  style={{ backgroundColor: "#ffc107", color: "white" }}
                  onClick={() => cancel()}
                >
                  Cancel
                </Button>,
                <Button
                  key="insert"
                  type="primary"
                  onClick={() => handleSubmit()}
                >
                  Save
                </Button>,
              ]
        }
      >
        <Form layout="vertical" autoComplete="off">
          <CSelectOption
            id="customer_id"
            validate={errors.customer_id}
            value={forms.customer_id}
            onChange={onCustomer}
            placeholder="Customer ID"
            label="Customer ID"
            data={listCustomer}
            required
            style={{ marginTop: -20 }}
          />
          <CSelectOption
            id="package_id"
            value={forms.package_id}
            validate={errors.package_id}
            onChange={onPackage}
            placeholder="Package ID"
            label="Package ID"
            data={listPackage}
            style={{ marginTop: -5 }}
            required
          />
          <CInput
            id="inv_number"
            validate={errors.inv_number}
            value={forms.inv_number}
            onChange={handleChange}
            placeholder="Invoice Number"
            label="Invoice Number"
            style={{ marginTop: -5 }}
            required
          />
          <CInput
            id="amount"
            value={forms.amount}
            placeholder="Amount"
            label="Amount"
            style={{ marginTop: -5 }}
            addonBefore="Rp."
            required
          />
          <CInput
            id="total_discount_amount"
            value={forms.total_discount_amount}
            onChange={onTotalDiscount}
            placeholder="Total discount amount"
            label="Total discount amount"
            style={{ marginTop: -5 }}
            addonBefore="Rp."
            required
          />
          <CInput
            id="total_amount"
            value={forms.total_amount}
            placeholder="Total amount"
            label="Total amount"
            style={{ marginTop: -5 }}
            addonBefore="Rp."
            required
          />
          <CDatePicker
            validate={errors.inv_date}
            required
            value={forms.inv_date}
            onChange={onChangeDate}
            label="Invoice Date"
            style={{ marginTop: -5 }}
            placeholder="Invoice Date"
          />
        </Form>
      </Modal>
    </>
  );
};

export default InvoiceLine;
