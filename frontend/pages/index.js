import { useEffect, useState } from "react";
import CTable from "../component/table";
import CTitle from "../component/title";
import { MyApi } from "./api/hello";

export default function Home() {
  const [paginate, setPaginate] = useState({
    current_page: "",
    pageSize: 15,
    keyword: "",
  });
  useEffect(() => {
    getData();
  }, [paginate]);
  const [data, setData] = useState();
  const [loading, setLoading] = useState(true);
  const [forms, setForms] = useState({
    id: "",
    name: "",
    email: "",
    phone: "",
    address: "",
    total: 0,
  });

  function onShowSizeChange(current, pageSize) {
    setPaginate({
      ...paginate,
      pageSize: pageSize,
      current_page: current,
    });
  }

  const getData = () => {
    setLoading(true);
    MyApi.get("customer", {
      params: {
        page: paginate.current_page,
        keyword: paginate.keyword,
        per_page: paginate.pageSize,
      },
    })
      .then((r) => {
        setData(r.data.data);
        setForms({
          ...forms,
          total: r.data.total,
        });
      })
      .catch((err) => {
        console.log(err.response);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const columns = [
    {
      title: "Name",
      key: "name",
      dataIndex: "name",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Phone",
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
  ];
  function handleSearch(e) {
    setForms({
      ...forms,
      [e.target.name]: e.target.value,
    });
    setPaginate({
      ...paginate,
      [e.target.name]: e.target.value,
    });
  }

  const refresh = () => {
    setPaginate({
      current_page: "",
      pageSize: 15,
      keyword: "",
    });
    getData();
  };

  return (
    <>
      <CTitle title="Customer" />
      <CTable
        name="Customer"
        total={forms.total}
        onShowSizeChange={onShowSizeChange}
        data={data}
        scroll={{ y: 300 }}
        columns={columns}
        searchValue={paginate.keyword}
        handleSearch={handleSearch}
        refresh={() => refresh()}
        loading={loading}
      />
    </>
  );
}
