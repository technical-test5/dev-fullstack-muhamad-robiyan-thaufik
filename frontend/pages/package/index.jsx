import React, { useState, useEffect } from "react";
import CTable from "../../component/table";
import CTitle from "../../component/title";
import { MyApi } from "../api/hello";

const Package = () => {
  const [paginate, setPaginate] = useState({
    current_page: "",
    pageSize: 15,
    keyword: "",
  });
  useEffect(() => {
    getData();
  }, [paginate]);
  const [data, setData] = useState();
  const [loading, setLoading] = useState(true);
  const [forms, setForms] = useState({
    name: "",
    description: "",
    amount: "",
    image: "",
    total: 0,
  });

  function onShowSizeChange(current, pageSize) {
    setPaginate({
      ...paginate,
      pageSize: pageSize,
      current_page: current,
    });
  }

  const getData = async () => {
    setLoading(true);
    await MyApi.get("package", {
      params: {
        page: paginate.current_page,
        keyword: paginate.keyword,
        per_page: paginate.pageSize,
      },
    })
      .then((r) => {
        setForms({
          ...forms,
          total: r.data.total,
        });
        setData(r.data.data);
      })
      .catch((err) => {
        console.log(err.response);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const columns = [
    {
      title: "Name",
      key: "name",
      dataIndex: "name",
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount",
      render: (item) => {
        return (
          "Rp. " + item.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
        );
      },
    },
    {
      title: "Descrtiption",
      dataIndex: "description",
      key: "description",
    },
  ];
  function handleSearch(e) {
    setForms({
      ...forms,
      [e.target.name]: e.target.value,
    });
    setPaginate({
      ...paginate,
      [e.target.name]: e.target.value,
    });
  }

  const refresh = () => {
    setPaginate({
      current_page: "",
      pageSize: 15,
      keyword: "",
    });
    getData();
  };

  return (
    <>
      <CTitle title="Package" />
      <CTable
        name="Package"
        total={forms.total}
        onShowSizeChange={onShowSizeChange}
        data={data}
        scroll={{ y: 300 }}
        columns={columns}
        searchValue={paginate.keyword}
        handleSearch={handleSearch}
        refresh={() => refresh()}
        loading={loading}
      />
    </>
  );
};

export default Package;
