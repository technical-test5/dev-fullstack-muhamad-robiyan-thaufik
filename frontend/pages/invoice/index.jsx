import { useEffect, useState } from "react";
import CTable from "../../component/table";
import CTitle from "../../component/title";
import { MyApi } from "../api/hello";

const Invoice = () => {
  const [paginate, setPaginate] = useState({
    current_page: "",
    pageSize: 15,
    keyword: "",
  });

  useEffect(() => {
    getData();
  }, [paginate]);

  const [data, setData] = useState();
  const [loading, setLoading] = useState(true);
  const [forms, setForms] = useState({
    id: "",
    customer_id: "",
    package_id: "",
    inv_number: "",
    inv_date: "",
    amount: 0,
    total_discount_amount: 0,
    total_amount: 0,
    invoice_line_id: "",
    total: 0,
  });

  function onShowSizeChange(current, pageSize) {
    setPaginate({
      ...paginate,
      pageSize: pageSize,
      current_page: current,
    });
  }

  const getData = () => {
    setLoading(true);
    MyApi.get("invoice", {
      params: {
        page: paginate.current_page,
        keyword: paginate.keyword,
        per_page: paginate.pageSize,
      },
    })
      .then((r) => {
        setData(r.data.data);
        setForms({
          ...forms,
          total: r.data.total,
        });
      })
      .catch((err) => {
        console.log(err.response);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const columns = [
    {
      title: "Customer Name",
      key: "name",
      dataIndex: "name",
    },
    {
      title: "Invoice Number",
      dataIndex: "inv_number",
      key: "inv_number",
    },
    {
      title: "Invoice Date",
      dataIndex: "inv_date",
      key: "inv_date",
    },
    {
      title: "Amount",
      dataIndex: "amount",
      key: "amount",
    },
    {
      title: "Total Discount Amount",
      dataIndex: "total_discount_amount",
      key: "total_discount_amount",
    },
    {
      title: "Total Amount",
      dataIndex: "total_amount",
      key: "total_amount",
    },
  ];

  const refresh = () => {
    setPaginate({
      current_page: "",
      pageSize: 15,
      keyword: "",
    });
    getData();
  };

  return (
    <>
      <CTitle title="Invoice" />
      <CTable
        name="Invoice"
        total={forms.total}
        onShowSizeChange={onShowSizeChange}
        data={data}
        scroll={{ y: 300 }}
        columns={columns}
        searchValue={paginate.keyword}
        refresh={() => refresh()}
        loading={loading}
      />
    </>
  );
};
export default Invoice;
