import { ControlOutlined, DownOutlined } from "@ant-design/icons";
import { Dropdown, Menu, Button } from "antd";

const CDropdownTable = ({ action }) => {
  return (
    <Dropdown
      overlay={
        <Menu>
          {action.map((item) => {
            return (
              <Menu.Item onClick={item.action} key={item.name}>
                {item.icon} {item.name}
              </Menu.Item>
            );
          })}
        </Menu>
      }
      placement="bottomRight"
    >
      <Button type="primary" size="small">
        <ControlOutlined />
        <DownOutlined />
      </Button>
    </Dropdown>
  );
};

export default CDropdownTable;
