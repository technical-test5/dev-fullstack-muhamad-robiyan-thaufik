import Head from "next/head";

const CTitle = ({ title }) => {
  return (
    <Head>
      <title>{title}</title>
    </Head>
  );
};

export default CTitle;
