import { Row, Spin, Col, Typography } from "antd";

const { Text } = Typography;
const Loading = () => {
  return (
    <Row
      justify="center"
      style={{
        margin: "auto",
        alignContent: "center",
        height: "100vh",
      }}
    >
      <Spin size="large" />
      <Col span={24} />
      <Text>Loading...</Text>
      <Col span={24} />
      <Text>Please Wait...</Text>
    </Row>
  );
};
export default Loading;
