import { Form, Input, InputNumber } from "antd";

const CInputNumber = ({
  label,
  name,
  value,
  id,
  onChange,
  placeholder,
  require,
  disabled,
  validate,
  ...rest
}) => {
  return (
    <>
      <Form.Item
        label={label}
        name={name}
        {...rest}
        validateStatus={validate ? "error" : null}
        help={validate}
      >
        <Input
          value={value}
          addonBefore="Rp."
          disabled={disabled}
          defaultValue={0}
          placeholder={placeholder}
          id={id}
          style={{ width: "100%" }}
          onChange={onChange}
        />
      </Form.Item>
    </>
  );
};

export default CInputNumber;
