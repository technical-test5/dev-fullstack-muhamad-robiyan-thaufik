import { Col, Row, Typography } from "antd";

const { Text } = Typography;
const ListGrey = ({ label, value, ...rest }) => {
  return (
    <Row
      {...rest}
      style={{
        backgroundColor: "#d3d3d3",
        display: "flex",
        alignItems: "center",
        width: "100%",
        paddingTop: 3,
        paddingBottom: 3,
      }}
    >
      <Col span={6}>
        <Text strong style={{ marginLeft: 10 }}>
          {label}
        </Text>
      </Col>
      <Col span={18}>
        <Text strong>{value}</Text>
      </Col>
    </Row>
  );
};

export default ListGrey;
