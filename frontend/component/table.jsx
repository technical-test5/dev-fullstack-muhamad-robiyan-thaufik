import {
  DownOutlined,
  PlusOutlined,
  SearchOutlined,
  SettingOutlined,
  SyncOutlined,
} from "@ant-design/icons";
import { Form } from "antd";
import {
  Table,
  Col,
  Pagination,
  Card,
  Row,
  Dropdown,
  Button,
  Input,
  Tooltip,
  Typography,
} from "antd";
import CDropdown from "./dropdown";

const { Title } = Typography;
const CTable = ({
  onShowSizeChange,
  name,
  subname,
  dropdown,
  searchValue,
  handleSearch,
  refresh,
  columns,
  data,
  total,
  scroll,
  loading,
  addData,
}) => {
  const [form] = Form.useForm();
  return (
    <Col span={24} style={{ backgroundColor: "#1890ff", paddingTop: "2px" }}>
      <Card hoverable="true">
        <Row>
          <Col span={24}>
            <Title level={3}>
              {name} {subname ? "/" : null} {subname}
            </Title>
          </Col>
          <Col span={24}>
            <Row style={{ float: "right" }}>
              <Form form={form} layout="inline">
                <Form.Item>
                  {dropdown ? (
                    <Dropdown
                      overlay={<CDropdown dropdown={dropdown} />}
                      placement="bottomRight"
                    >
                      <Button type="primary" size="small">
                        <SettingOutlined></SettingOutlined>
                        Action
                        <DownOutlined></DownOutlined>
                      </Button>
                    </Dropdown>
                  ) : addData ? (
                    <Button onClick={addData} type="primary" size="small">
                      <PlusOutlined />
                      Add Data
                    </Button>
                  ) : null}
                </Form.Item>
                {handleSearch ? (
                  <Form.Item>
                    <Input
                      size="small"
                      placeholder="Search"
                      name="keyword"
                      id="keyword"
                      value={searchValue}
                      onChange={handleSearch}
                      prefix={
                        <Tooltip title="Search">
                          <SearchOutlined
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        </Tooltip>
                      }
                    />
                  </Form.Item>
                ) : null}
                <Form.Item onClick={refresh}>
                  <Tooltip title="Reload">
                    <Button size="small">
                      <SyncOutlined />
                    </Button>
                  </Tooltip>
                </Form.Item>
              </Form>
            </Row>
          </Col>
          <Col span={24} style={{ paddingTop: "20px" }}>
            <Table
              columns={columns}
              dataSource={data}
              scroll={scroll}
              size="small"
              rowKey="id"
              pagination={false}
              loading={loading ? true : false}
            />
          </Col>
          <Col span={24} style={{ paddingTop: "10px" }}>
            <Pagination
              style={{ float: "right" }}
              showSizeChanger
              onShowSizeChange={onShowSizeChange}
              defaultCurrent={1}
              total={total}
              defaultPageSize={15}
              pageSizeOptions={["15", "30", "50", "100", "200"]}
              onChange={onShowSizeChange}
            />
          </Col>
        </Row>
      </Card>
    </Col>
  );
};
export default CTable;
