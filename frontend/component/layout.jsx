import { Layout } from "antd";
import { useState } from "react";
import TheHeader from "./header";
import Sidebar from "./sidebar";

const { Content } = Layout;
const CLayoutAdmin = ({ children }) => {
  const [collapsed, setCollapsed] = useState(false);

  const toggle = () => {
    setCollapsed(!collapsed);
  };

  return (
    <Layout>
      <TheHeader toggle={toggle} collapsed={collapsed} />
      <Layout>
        <Sidebar toggle={toggle} collapsed={collapsed} />
        <Layout>
          <Content
            style={{
              margin: "20px 5px",
              overflow: "auto",
              height: "90vh",
              marginTop: "80px",
              marginLeft: "15px",
              marginRight: "15px",
            }}
          >
            {children}
            <div style={{ marginBottom: "1.5%" }} />
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
};

export default CLayoutAdmin;
