<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $data = DB::table('customers')
        ->select('id','name','email','phone','address')
        ->when($request->keyword, function ($q) use ($request) {
            $q->where('name', 'ILIKE', "%{$request->keyword}%")
            ->orWhere('address', 'ILIKE', "%{$request->keyword}%")
            ->orWhere('email', 'ILIKE', "%{$request->keyword}%")
            ->orWhere('phone', 'ILIKE', "%{$request->keyword}%");
        })
        ->orderBy('name', 'ASC')
        ->paginate($request->per_page);

        return response()->json($data);
    }

    public function getList()
    {
        $list = DB::table('customers')
        ->select('id','name')
        ->orderBy('name', 'ASC')
        ->get();

        return response()->json($list);
    }
}
