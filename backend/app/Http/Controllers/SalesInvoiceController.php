<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalesInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = DB::table('sales_invoices')
        ->select(
            'sales_invoices.id',
            'customers.name',
            'sales_invoices.inv_number',
            'sales_invoices.inv_date',
            'sales_invoices.amount',
            'sales_invoices.total_discount_amount',
            'sales_invoices.total_amount',
        )
        ->join('customers', 'sales_invoices.customer_id', '=', 'customers.id')
        ->orderBy('inv_date', 'DESC')
        ->paginate($request->per_page);

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data =DB::table('sales_invoices')
        ->select(
            'sales_invoices.id',
            'customers.name',
            'sales_invoices.inv_number',
            'sales_invoices.inv_date',
            'sales_invoices.amount',
            'sales_invoices.total_discount_amount',
            'sales_invoices.total_amount',
        )
        ->where('sales_invoices.id', $id)
        ->join('customers', 'sales_invoices.customer_id', '=', 'customers.id')
        ->get();

        return response()->json($data);
    }
}
