<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PackageController extends Controller
{
    public function index(Request $request)
    {
        $data = DB::table('packages')
        ->select('id','name','description','amount')
        ->when($request->keyword, function ($q) use ($request) {
            $q->where('name', 'ILIKE', "%{$request->keyword}%");
        })
        ->orderBy('name', 'ASC')
        ->paginate($request->per_page);

        return response()->json($data);
    }


    public function show($id)
    {
        $data = Package::find($id)->amount;

        return response()->json($data);
    }



    public function getList()
    {
        $list = DB::table('packages')
        ->select('id','name')
        ->orderBy('name', 'ASC')
        ->get();

        return response()->json($list);
    }
}
