<?php

namespace Database\Seeders;

use App\Models\Customer;
use Illuminate\Database\Seeder;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 500; $i++) {
            $data[$i] = [
                'email' => "customer$i" . "@gmail.com" ,
                'name' => "Name $i",
                'phone' => "03111234$i",
                'address' => "Jln. Melati No. $i",
            ];
        }
        Customer::insert($data);
    }
}
